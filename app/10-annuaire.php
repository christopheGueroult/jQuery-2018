<?php
require('./connect.php');

// récupérer les contenteditables depuis bdd 
function getAnnuaire($mysql){

  $req = $mysql->query("SELECT * FROM annuaire");
  $content = $req->fetchAll();
  return $content;

}

$contenus = getAnnuaire($mysql);
var_dump($contenus);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <meta name="description" content="une super formation jQuery avec Dany Brioche">
    <link rel="stylesheet" href="css/global.css">
</head>

<body>
    
    <!-- annuaire -->
    <div class="container">
        <div class="annuaire">

            <div class="success"></div>
            <div class="alert"></div>

            <div class="row head">
                <div class="col-xs-1">
                    <p>Id</p>
                </div>
                <div class="col-xs-3">
                    <p>Nom et prénom</p>
                </div>
                <div class="col-xs-4">
                    <p>Email</p>
                </div>
                <div class="col-xs-2">
                    <p>Tel</p>
                </div>
                <div class="col-xs-2">
                    <p>Action</p>
                </div>
            </div>

            <?php  
                $nbRows = count($contenus);
                for($i = 0; $i < $nbRows; $i++) {
            ?>

            <div class="row line">
                <div class="col-xs-1">
                    <p data-id><?php echo $contenus[$i]['id']; ?></p>
                </div>
                <div class="col-xs-3">
                    <p contenteditable="false"><?php echo $contenus[$i]['name']; ?></p>
                </div>
                <div class="col-xs-4">
                    <p contenteditable="false"><?php echo $contenus[$i]['email']; ?></p>
                </div>
                <div class="col-xs-2">
                    <p contenteditable="false"><?php echo $contenus[$i]['tel']; ?></p>
                </div>
                <div class="col-xs-2">
                    <p>
                        <span class="icon-border_color"></span>
                        <span class="icon-check"></span>
                        <span class="icon-delete_forever"></span>
                    </p>
                </div>
            </div>

            <?php  
            }
            ?>
        </div>
    </div>
    <!-- /annuaire -->

    <script src="js/production.js"></script>
</body>

</html>