'use strict';

$(function(){ 
    // passe les contenteditable d'une ligne à true
    editLine = (elem) => {
        const line = elem
            .parent('p')
            .parent('div')
            .parent('.line');

        line
            .find('[contenteditable]')
            .each(function() {
                $(this).attr('contenteditable', 'true');
            });
    }

    // récupère les contenteditable et update en bdd
    updateLine = (elem) => {
        let contents = new Array();

        const line = elem
            .parent('p')
            .parent('div')
            .parent('.line');

        line
            .find('[contenteditable]')
            .each(function(index, contenu) {
                console.log(index, contenu);
                // console.log(contenu.textContent);
                contents.push(contenu.textContent);
            });
        
        console.log(contents);

        const id = parseInt(
            line
            .find('[data-id]')
            .text()
        );

        console.log(id);

        $.ajax({
            method: 'POST',
            url: 'annuaire-update.php',
            data: {
                id: id,
                line: contents
            }
        }).done(function(msg) {
            if (msg === 'yes') {
                $('.annuaire .success').text('update successfully').show();
            } else {
                $('.annuaire .alert').text('Echec update !').show();
            }
        });
    }

    deleteLine = (elem) => {
        const line = elem
            .parent('p')
            .parent('div')
            .parent('.line');

        const id = parseInt(
            line
            .find('[data-id]')
            .text()
        );

        $.ajax({
            method: 'POST',
            url: 'annuaire-delete.php',
            data: {
                id: id
            }
        }).done(function(msg) {
            if (msg === 'yes') {
                $('.annuaire .success').text('update successfully').show();
            } else {
                $('.annuaire .alert').text('Echec update !').show();
            }
        });

    }

    // Quand on clique sur l'icon d'édition on veut passer
    // les contenteditable de la ligne à true
    $('.annuaire .icon-border_color').click(function() {
        editLine( $(this) );
    });

    // Quand on clique sur le btn pour valider, on veut récupérer les textes de la ligne
    // et faire l'update en bdd
    $('.annuaire .icon-check').click(function() {
        updateLine( $(this) );
    });

    // Quand on clique sur icon delete, on veut récupérer l'id de la ligne et supprimer 
    // cette ligne en bdd
    $('.annuaire .icon-delete_forever').click(function() {
        deleteLine( $(this) );
    });

}); // pas supprimé svp !