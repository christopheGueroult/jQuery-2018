$(function(){ 

    $('button:nth-child(1)').click(function(){
        $('ul').before('<p>Vive la Bretagne et les bretons ! </p>');
    });

    $('button:nth-child(2)').click(function(){
        $('ul').after('<p>Vive la Bretagne et les bretonnes ! </p>');
    });

    $('button:nth-child(3)').click(function(){
        $('ul').append('<li>Vivre les bretons transgenre </li>');
    });

    $('button:nth-child(4)').click(function(){
        $('ul').prepend('<li>Vivre les bretons sgbaalllddgt </li>');
    });

    
    // Exercise
    // Pour le btn 5, quand on clique dessus on veut ajouter un li avec item 1
    // Pour le btn 6, quand on clique dessus on veut ajouter un li avec item 5
    $('button:nth-child(5)').click(function(){
        $('ul').prepend('<li>item 1</li>');
    });

    $('button:nth-child(6)').click(function(){
        $('ul').append('<li>item 5</li>');
    });

    // amélioration : quand on ajoute un item au debut de la liste, si on tente 
    // d'aller en dessous de item 1 une alert affiche un message d'erreur
    // quand on ajoute un item en fin de liste, on demmare à 5 et on incrémente 
    // si on tente d'aller au delà de 10 un message d'erreur s'affiche via un alert

    let i = 1;
    let name = "ChrisToPhe";
     // une variable qui commence à 1
    $('button:nth-child(7)').click(function(){
        // tester si variable === 1
            // ajouter un li avec item + mavariable 
            // on décrémente mavariable
        // else
            // alert(message d'erreur)
        
        if (i === 1) {
            $('ul').prepend('<li>item 1</li>');
            i --;
        } else {
            alert(`Sorry, you can't add item ${name.toLowerCase()} before 1`);
        }
    });

    let c = 5;
    $('button:nth-child(8)').click(function(){
        if (c <= 10) {
            $('ul').append(`<li>item ${c}</li>`);
            c++;
        } else {
            alert(`Sorry, you can't add item after 10`);
        }
    });

    $('button:nth-child(9)').click(function(){
        // récupérer dernier caractère dans li:first-child 
        // caster ce caractère en number
        // si ce number est > O
            // on ajoute un li devant avec item + mavar
        // else 
            // message d'erreur
        let n = $('ul li:first-child').text().substr( $('ul li:first-child').text().length -1);
        n = parseInt(n);
        if( n > 1 ) {
            $('ul').prepend(`<li>item ${n-1}</li>`);
        } else {
            alert(`Sorry, you can't add item before 0`);
        }
    });

    $('button:nth-child(10)').click(function(){
        let n = $('ul li:last-child').text().substr( $('ul li:last-child').text().length -2);
        n = parseInt(n);
        if( n < 10 ) {
            $('ul').append(`<li>item ${n+1}</li>`);
        } else {
            alert(`Sorry, you can't add item after 10`);
        }
    });



 }); // pas supprimé svp !

 // git ignore pour les node_modules et push sur github
 // before et after avec exo
 // attaque tp jusqu'à la fin