$(function(){ 

    $('button:nth-child(1)').click(function(){
        let recup = $('ul li:nth-child(2)').text(); // getter
        $('p').text(recup); // setter
    });

    $('button:nth-child(2)').click(function(){
        $('p').text('toujour rien pour l\'instant !'); // setter
    });

    $('button:nth-child(3)').click(function(){
        let content = $('ul').html(); // getter
        alert(content) // setter
    });

    $('button:nth-child(4)').click(function(){
        $('p').html('<strong>toujour rien pour l\'instant !</strong>'); // setter
    });

    $('button:nth-child(5)').click(function(){
        let srcImg = $('.img1').attr('src'); // getter
        console.log(srcImg);
    });

    $('button:nth-child(6)').click(function(){
        $('.img1').attr('src', 'img/gal3.jpg'); // setter
    });

    $('button:nth-child(7)').click(function(){
        let val = $('#name').val(); // getter
        console.log(val);
    });

    $('button:nth-child(8)').click(function(){
        $('#name').val('Cédric'); // setter
    });

    // Exercice 
    // Quand on clique sur l'img1, on affiche gal1.jpg et gal1.jpg
    // Quand on clique sur l'img2, on affiche gal2.jpg et gal2.jpg
    // $('.img1').click(function(){
    //     $('.img1').attr('src', 'img/gal1.jpg'); // setter
    //     $('.img2').attr('src', 'img/gal1.jpg'); // setter
    // });

    // $('.img2').click(function(){
    //     $('.img1').attr('src', 'img/gal2.jpg'); // setter
    //     $('.img2').attr('src', 'img/gal2.jpg'); // setter
    // });

    // Exercice 2 
    // Quand on clique sur img1 on affiche img1 dans img3 (récupérer gal1.jpg dynamiquement)
    // Quand on clique sur img2 on affiche img2 dans img3 (récupérer gal2.jpg dynamiquement)
    $('img').click(function(){
        if (!$(this).hasClass('img3')) {
            // get attr src de $(this)
            let src = $(this).attr('src');
            // set attr src de $('.img3')
            $('.img3').attr('src', src);
        }
    });

    // stop propagation 
    // quand on clique sur un li, on affiche son contenu html dans une alert
    $('li').click(function(e){
        e.stopPropagation();
        alert( $(this).html() );
    });

    

 }); // pas supprimé svp !

 // git ignore pour les node_modules et push sur github
 // before et after avec exo
 // attaque tp jusqu'à la fin