$(function(){ 

    // cibler un element du dom et invoquer une méthode sur cet element
    // $('selecteur css').methodeJQuery(1 ou plusieurs args voir doc);

    // écouteur d'event sur un objet du dom
    // $('selecteur css').evenement(function(){
        // code à executer quand on declanche l'evenement sur cet objet
    // });

    $('p').css(
        {
            'background': 'red', 
            'color': 'blue', 
            'fontStyle': 'bold'
        }
    );

    $('button:nth-of-type(1)').click(function(){
        $('p').hide(3000);
    });

    // const elem = $('#btn').methode();
    // for (let i = 0; i < 5000 ; i++) {
    //     // elem.methode();
    // }
    // ici on utilise une reference dans la boucle, ainsi le navigateur n'a pas besoin 
    // de parcourir le dom 5000 x à la recherche d'un id

    $('button:first-child').click(function(){
        alert($(this).text());
        // text() get text inside élément ciblé 
        // $(this) fait référence à ce bouton, celui sur lequel on clique
    });

    $('#btn').dblclick(function(){
        alert('Wouhouuuuu, tu as double cliqué !');
    });

    $('button:nth-child(3)').mouseenter(function(){
        alert('Hummmm tu es bien rentré ');
    });

    $('button:nth-child(4)').mouseleave(function(){
        alert('bouuuuuu, tu m\'as quitté ');
    });

    $('button:nth-child(5)').hover(function(){
        console.log('ça se declanche quand tu rentre sur le bouton ');
    }, function(){
        console.log('ça se declanche quand tu quitte sur le bouton ');
    });


 }); // pas touche !