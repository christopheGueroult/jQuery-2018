<?php
require('./connect.php');
if (isset($_POST)) {
    var_dump($_POST);

    $sql = "UPDATE annuaire SET name = :name, 
            email = :email, 
            tel = :tel,  
            WHERE id = :id";

    $stmt = $mysql->prepare($sql); 

    $stmt->bindParam(':name', $_POST['name'], PDO::PARAM_STR); 
    $stmt->bindParam(':email', $_POST['email'], PDO::PARAM_STR);   
    $stmt->bindParam(':tel', $_POST['tel'], PDO::PARAM_STR);   
    $stmt->bindParam(':id', $_POST['id'], PDO::PARAM_INT);   

    if ($stmt->execute()) {
        echo 'yes';
    } else {
        echo 'no';
    }
}
?>